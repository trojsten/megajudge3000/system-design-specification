.. Megajudge3000 documentation master file, created by
   sphinx-quickstart on Mon Feb 24 12:02:45 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Megajudge 3000
==============

Megajudge 3000 (M3K) is a simple-to-deploy, scalable system for automated testing of submissions in
programming competitions or programming courses.

This repository contains a "system design specification" of this system for now. It will naturally
morph into documentation of the system as it is implemented.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   specification/entity-diagram/index
   specification/db-model/index
   specification/usecases-and-us/index
   specification/apis/index


Changelog
---------

.. changelog::
    :version: 1.1.0
    :released: 2020-03-07

    .. change::
        :tags: general

      After review of available message queues or other work distribution systems, it appears that
      custom implementation will be the best option for us. DB models and use-cases were updated
      to describe our implementation, and API for the worker was designed.

.. changelog::
    :version: 1.0.0
    :released: 2020-03-02

    .. change::
        :tags: general

      Initial version


Basic description of this system
--------------------------------

These components provide a web interface for organizers of programming competitions or teachers, as
well as APIs for competition-specific webs. The basic use-case would be best described by a concrete
example:

KSP (Korešpondenčný Seminár z Programovania) is a programming competition for high-school students.
These students solve tasks by writing a program, and submit this program to the KSP webpage (called
Trojstenweb). These programs need to be tested (for correctness, and whether they are sufficiently
efficient). Goal of the M3K is to provide a system and API that can be used by Trojstenweb (and
other competition-specific webpages) for testing submits such as these.

The "web application" part of this system provides an interface for competition organizers so that
they can create, manage and debug tasks to be tested on this system. Additionally it provides
"insights" into what the contenders are submitting, and more. See :ref:`Use-cases` more in-depth
basic description of use-cases and :ref:`User-stories` for possibilities for further advanced
features this system may support.


Technical requirements
----------------------

This system consists of several parts, which are described more in-depth in section :ref:`Entity
diagram`. Technological requirements for deployment these parts are as follows:

  - "Control stack"

    - For local use-case, you will need a host with Docker and some sort of orchestrator, such as
      Docker Swarm
    - For cloud use-case, you will need a SQL database compatible with Django ORM, some form of
      cloud storage, such as GCS or S3, and a way to host a Docker containers, such as managed
      Kubernetes cluster or a Flexible environment in an AppEngine.
    - For development, `docker-compose` is sufficient.

  - To run a worker, you will need a sufficiently powerful physical or virtual machine.

The application will support all modern browsers (Chrome, Firefox, Edge, …) in their current and
previous major versions. In a further iteration, support for mobile browsers and mobile UX
optimization will be added.


Specifics for subject "2-INF-145: Tvorba internetových aplikácií"
-----------------------------------------------------------------

Our first priority is to create a functioning, practically and easily usable judge system which is
cheap to maintain, simple to integrate with other competition webs and easy to develop further by
our successors.

Our second priority is to submit a **part** of this system as a project for subject "2-INF-145:
Tvorba internetových aplikácií". Whenever these are in conflict, development of a practically usable
system will get the priority.

Scope of project for this subject is the Backend and Frontend of this system. In particular, workers
are considered to be out-of-scope, despite the fact that they communicate over the internet with the
Backend. See :ref:`Entity diagram` for further reference.


Used technologies
`````````````````

Backend will be written in Python and will use Django. The Frontend will be written in TypeScript
and will use React. For development, sqlite database will be used. Other 3rd party libraries will be
used as necessary.

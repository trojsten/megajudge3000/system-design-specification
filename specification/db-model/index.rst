Database model
==============

The system will use a PostgreSQL relational database. The following diagram shows the principal
schema of this database. Exact implementation may differ slightly due to ORM technicalities and
custom data structures of 3rd party Django apps.

.. image:: dbmodel.svg


Description of tables
---------------------

Competitions
````````````

A basic unit for which the M3K provides services is a single Competition. From the point of view of
M3K, a competition is a container which contains tasks that can be tested. One competition has
exactly one API key, which a competition web must use to authorize itself when sending submits to be
tested to the system, and a URL of a webhook, to which a request is sent when testing finishes.


Users
`````

A table of users. User may be a Superadmin, which grants all privileges in the system to the
user, or they can be a Competition Admin for one or more competitions (see :ref:`Use-cases` for
visualization). Auxiliary table "Competition_User" implements this many-to-many relationship.


Tasks
`````

Competitions contain tasks that can be tested. These tasks can be created or edited by Competition
administrators. A task has an alphanumeric identifier ``id``, and URL of all assets needed to test
the task - such as inputs/outputs, or "Custom Execute" scripts. Field ``test_type`` determines what
testing method will be used when testing the task.


TestType
''''''''

  - ``execute_and_diff``: The classic way of testing submits - submit assets are prepared for
    testing (by e.g. compiling them), and are executed on a prepared set of inputs. The outputs are
    then compared with expected outputs.
  - ``execute_custom``: The most flexible way of testing submits - submit assets are prepared for
    testing, and testing control is then passed to a custom script which may do whatever it needs to
    do to test the submit.


Batch and BatchConfig
`````````````````````

If ``TestType`` is ``execute_and_diff``, the task will be tested in batches. Each batch has a name,
and has at least one associated BatchConfig. The BatchConfig specifies what ``time_limit`` or
``memory_limit`` will be enforced during testing. A BatchConfig may be different for different types
of submitted assets (Python may need different time limits than C++). A batch MUST contain exactly
one BatchConfig, where ``submit_asset_type`` is NULL.


Submits
```````

Submit is a single testable unit. ``competition_specific_id`` is a unique identifier of the submit
for the given task (this allows the competition web to assign its own reference ID to a submit,
which helps with backwards compatibility). ``priority`` is a numeric field which can be used to test
some submits "sooner" than others (e.g. during emergency debugging of a task during a competition).
``asset_type`` specifies what was submitted for testing (if source code then what language, etc).
Lastly, ``submit_assets_url`` contains a location of submitted assets in the storage.


SubmitTag
`````````

When creating a submit, a competition can "tag" this submit using a key-value pair specific for that
competition. Example use-case would be tagging a submit with ``tag_name`` "submit_author_username",
where ``tag_value`` would be name of the user on the Competition web whose submit this is. This
allows us to implement advanced querying of submits (as described in some of the
:ref:`User-stories`) based on user name of the submitter, or other competition-specific submit
property.


Workers
```````

Holds workers associated with this controller:

  - ``stopped`` (boolean): The admin can temporarily stop a worker from accepting jobs.
  - ``auth_token`` (string): This token must be included with every API request the Worker makes.
  - ``last_contact`` (time): Time of the last valid API call from the worker (useful to detect that
    a worker has died).


GenericJob
``````````

A schedulable job that can be executed by some Worker. Two types of jobs subclass this class
(multi-table inheritance): ``SubmitTestJob`` and ``PrepareAssetsJob``.

  - ``priority`` (int + constrained to values 0-4 inclusive): Job priority. Jobs with lower priority
    will be executed sooner.
  - ``created_at`` (time): When was this job created
  - ``scheduled_for`` (nullable time): When should this job be executed
  - ``Workers.id`` (nullable FK): Which worker is processing this job.

See :ref:`Scheduling jobs (MVI)` for how the job scheduling works and how these fields are used.


PrepareAssetsJob
````````````````

Represents a job that prepares testing assets for a given task - for example, when the Competition
Administrator submits a ``.zip`` file with generator sources and task configuration, we need to run
this generator and upload its outputs as expected outputs for the task.

  - ``asset_prep_log`` (JSONField - schema Logs): Logs from the asset preparation process.
  - ``asset_prep_time`` (time): How long did the asset preparation take.
  - ``original_asset_url`` (FileField): Original file submitted by the competition admin.


SubmitTestJob
`````````````

Represents a job that tests some Submit.

  - ``asset_prep_log`` (JSONField - schema Logs): Logs from submit asset preparation - such as
    submit compilation.
  - ``asset_prep_time`` (time): How long did it take to prepare assets for testing.
  - ``summary_result`` (string): Result of the whole testing. If ``TestType`` is
    ``execute_and_diff``, this string is one of the usual "OK", "WA", "EXC", "TLE", …. For
    ``execute_custom``, this can be an arbitrary string.


BatchResult
```````````

Result of testing of a single batch.

  - ``protocol`` (JSONField - schema BatchProtocol): Protocol from testing of the batch.
  - ``result`` (string): Result of testing of the batch (see ``summary_result`` in
    :ref:`SubmitTestJob`).


JSON Schemas
------------

Some fields are JSONFields, and data in those fields should follow defined schemas. This section
describes the schema.


Logs
````

.. code:: json

  {
      "$schema": "http://json-schema.org/draft-07/schema",
      "type": "array",
      "title": "Logs schema",
      "description": "Represents logs from the execution of some process",
      "items": {
          "$id": "#/items",
          "type": "object",
          "title": "Line schema",
          "description": "A single line of program output",
          "required": [
              "time",
              "stream",
              "text"
          ],
          "properties": {
              "time": {
                  "$id": "#/items/properties/time",
                  "type": "integer",
                  "description": "When was this line output (wall time in ms since the process started)"
              },
              "stream": {
                  "$id": "#/items/properties/stream",
                  "choices": ["STDOUT", "STDERR"],
                  "description": "Over which stream was the line output"
              },
              "text": {
                  "$id": "#/items/properties/text",
                  "type": "string",
                  "description": "The output line"
              }
          }
      }
  }


BatchProtocol
`````````````

.. code:: json

  {
      "$schema": "http://json-schema.org/draft-07/schema",
      "type": "array",
      "title": "Batch testing protocol",
      "description": "Protocol from testing of a single batch.",
      "items": {
          "$id": "#/items",
          "type": "object",
          "title": "Test result",
          "description": "Result of testing on a single sample",
          "examples": [
              {
                  "result": "OK",
                  "time": 0.0,
                  "name": "00.sample.a.in"
              }
          ],
          "required": [
              "name",
              "result",
              "time"
          ],
          "properties": {
              "name": {
                  "$id": "#/items/properties/name",
                  "type": "string",
                  "title": "Name of the test case",
                  "examples": [
                      "00.sample.a.in"
                  ]
              },
              "result": {
                  "$id": "#/items/properties/result",
                  "type": "string",
                  "title": "Test case result",
                  "examples": [
                      "OK",
                      "WA",
                      "TLE"
                  ]
              },
              "time": {
                  "$id": "#/items/properties/time",
                  "type": "integer",
                  "title": "Testing time",
                  "description": "How long did the testing of this sample run in ms",
                  "examples": [
                      0
                  ]
              }
          }
      }
  }

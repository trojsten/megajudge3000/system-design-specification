Worker API
==========

This API is used by workers to query the Controller for jobs and to report (partial or complete)
results.


.. http:get:: /jobs/next

   Get the next scheduled job.

   **Example request**:

   .. sourcecode:: http

      GET /jobs/next HTTP/1.1
      Host: example.com
      Accept: application/json
      Authorization: xoofohphiehohgh3zaise2Sapeen5bei3ne3iengairaetenoo5eecoh2Nephied

   **Example response**:

   .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json

      { ... JSON to be done ... }

   :reqheader Authorization: Authorization token of the runner
   :statuscode 200: A job is waiting to be processed and has been assigned to you.
   :statuscode 204: There is no job waiting in the queue.
   :statuscode 403: Authorization header is missing or is invalid.


.. http:patch:: /jobs/(int:job_id)/results

   Upload (potentially partial) testing results.

   **Example request**:

   .. sourcecode:: http

      PATCH /jobs/123/results HTTP/1.1
      Host: example.com
      Authorization: xoofohphiehohgh3zaise2Sapeen5bei3ne3iengairaetenoo5eecoh2Nephied

      { ... JSON to be done ... }

   **Example response**:

   .. sourcecode:: http

      HTTP/1.1 204 No Content
      

   :query job_id: ID of the job we are reporting results for
   :reqheader Authorization: optional OAuth token to authenticate
   :statuscode 204: Result successfully updated (no response needed)
   :statuscode 423: Results were previously marked as final, no further modification is permitted.
   :statuscode 403: Authorization header is missing or is invalid.


.. http:post:: /jobs/(int:job_id)/results

   Same thing as ``PATCH`` method described above, except that by using this request method, the
   worker indicates that the testing has finished, the results are final and may not be updated
   further. Any further (valid) ``PATCH`` or ``POST`` calls on this endpoint will return ``423
   Locked``.

Entity diagram
==============

The following diagram depicts actors and entities which comprise the M3K system.

.. image:: entity.svg


The "Controller Stack"
----------------------

The "Controller Stack" is a set of containers that are intended to be deployed to a Docker host (or
a Docker cluster with either Swarm or Kubernetes as the orchestrator) which provide all features
of the M3K, except for the workers.

It is also our intention that the sysadmin can pick and choose which containers are actually
deployed, which gives them the opportunity to replace some functions of the "Controller Stack" with
their cloud-native alternatives (Cloud SQL vs. PostgreSQL container, GCS or S3 storage instead of
MiniIO server).

This makes the "Controller Stack" usable as a cloud-native application, as well as simple to deploy
in a local Docker-based cluster.


Backend
```````

Backend implements the application logic of M3K. Its primary responsibility is to maintain data
structures which define various Competitions and Tasks, manage testing data related to the given
task, receive Submits from client webs, schedule them for testing, distribute work among Workers and
inform the client webs about test results. It stores its data structures in a SQL database (see also
:ref:`Database model`), and test assets of various tasks in Storage.


Frontend
````````

The frontend provides a simple and convenient interface for competition administrators to interact
with the Judge system. Superadmins can create new competitions, promote users to competition
administrators, and configure communication of competition-specific webs with the system.
Competition administrators can create and debug new tasks and upload related assets (test cases,
test case generators, …).

This interface also provides a live-overview of the whole system (worker status, which submits are
being processed, …).

Code of the frontend will be packaged in the same Docker image as the backend, but it will be
possible to spawn backend-specific and frontend-specific containers from this image to meet the
scaling demand. These containers can be deployed to a Docker-based cluster as part of a "Controller
Stack", or in a cloud-native way to managed cluster of even as Flexible instances of Google
AppEngine.


Database
````````

The necessary relational database may be provided by a PostgreSQL container in the "Controller
Stack", or by a cloud-native database such as Cloud SQL provided by GCP.


Storage
```````

It is necessary to store potentially large assets for each task (and possibly for each submit), such
as expected outputs of various test-cases. This storage must be cheap, and accessible by both the
Backend and the Worker, while keeping in mind the constraint that the worker communicates with the
"Controller Stack" only via HTTPS. For the sake of simplicity of deployment, this prevents us from
using solutions such as local filesystem or NFS mounts.

The exact implementation is still yet to be determined. Two likely candidates exist: cloud-native
solution, such as GCS Storage Buckets or Amazon S3 buckets (with local deployment case being handled
by a MiniIO server which would be a part of the Controller Stack), or a WebDAV server. We will
choose the final solution once we evaluate development and maintenance costs of all our options.


The Worker
----------

Worker is a (physical or virtual) machine where the Submits are tested. This VM contains Docker, and
"Worker daemon" runs as a Docker container. When a submit needs to be tested, a new "Testing
container" is spawned which provides an environment (such as compilers, …) where the testing runs.
Auxiliary services, such as "Asset cache" ("HTTP Cache" in the diagram) may run here as well.

Keeping our goal of simple deployment and easy maintenance in mind, the Worker runs a Fedore CoreOS.
FcOS is a minimal, self-updating operating system for running Docker containers. It can be
provisioned during installation by the means of an Ignition config. This means, that maintenance is
cheap, since the system updates itself, and deployment is easy, since defacto it is a single command
to boot a VM with install media and an Ignition config.


Worker daemon
`````````````

The Worker daemon receiver work from the Backend, downloads (and caches) submit and test assets,
creates a new "Testing container", prepares the submit for testing (e.g. by compiling the submitted
source code) and executes tests on this submit. During this process it sends progress updates to the
Backend.

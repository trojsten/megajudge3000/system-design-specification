Use-cases and user stories
==========================

This section documents basic use-cases of the system, and various user stories collected during
design phase of the system. Additionally, use-cases or user stories marked as "MVI" will be part
of a "Minimum Viable Implementation", which will be the result of the first development iteration.

The reason for including both the use-cases and the user stories is to avoid clunky user stories
describing the obvious (e.g. "As a competition administrator, I want my web to be able to submit a
task for testing, so that a task can be tested").


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   use-cases
   user-stories

User-stories
============

This page contains User Stories collected from various (current or future) M3K stakeholders
regarding the system. None of them will be included in the Minimum Viable Implementation, but they
affect design of the system from early stages, so that it is easy to implement them during later
development iterations.


Social-auth login
-----------------

I, as M3K user, want to login using an oAuth provider, so that I don't have to manage separate
credentials for this system.

Moreover, I as Superadmin of the system, want to allow users to create accounts for themselves, if
they register using an oAuth provider of a trusted G-Suite domain (such as @trojsten.sk).


Viewing submits per user
------------------------

As a Competition Admin, I want to have the ability to filter submits together with their
corresponding protocols, and view only submits of a certain user a given competition, so that I may
get better understanding of behavior of that user.


Self-tests for a task
---------------------

As a Competition Admin, I want the system to have the ability to include sample submissions as task
assets, and to automatically re-test these samples after any of the task configuration parameters
have been changed, so that I may catch issues with aging tasks more efficiently.


Re-test of submits after task change
------------------------------------

As a Competition Admin, I want to retest all submits for a given task after the task configuration
changes, so that I may fix issues with the task and update scores for already existing submits.


Live status board
-----------------

As a Competition Admin, I want to be able to see live status of the test queue for my competition.
Additionally, I want to be able to see a live status overview of the whole M3K system so that I may
quickly diagnose issues with submit testing.


Import task from Polygon
------------------------

As a competition administrator, I want a simple way to import programming contest problem I've
created in Polygon (polygon.codeforces.com), so that I don't have to download and prepare these
assets manually.


Advanced submit queries and query actions
-----------------------------------------

As a competition administrator I want to have the option to create advanced queries for submits (for
example, select all TLE submits for competition XYZ created by users from country Slovakia between
9:00AM today and now). Moreover, I want to be able to execute actions, such as rejudge, on these
submits.


Public live views
-----------------

As a competition administrator, I want to be able to create a public page, accessible over a shared
link, which will provide a live-view of high-level status of all submits that match a certain query,
so that I may share this page with spectators during a competition. Moreover, I optionally want this
page to automatically disappear after a given time so that I don't have to clean it up manually.

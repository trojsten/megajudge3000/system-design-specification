Use-cases
=========

This section describes the most basic use-cases of the M3K system. It will walk you through initial
use-cases and describe features you will use when you wish to integrate the M3K system with your
competition so that you can create tasks and test submits.

.. image:: use-case-diagram.svg


Creating a competition (MVI)
----------------------------

First, you must implement support for the M3K Competition API in your competition web. Then you need
to ask a Superadmin of the M3K system to create a new competition. You will need to provide the
Superadmin with URL of a webhook where the system can notify your application of new events and in
turn, the Superadmin will provide you with an API key which your application can use to authorize
its requests to the M3K system.


Creating a user (MVI)
---------------------

Next, we need a user to manage tasks in the newly created competition. The system provides no option
for public registration, so the account must be created by the Superadmin. The Superadmin can then
also grant "Competition admin" privileges to a user (either one which already exists, or a new one)
for the given competition.


Creating a task (MVI)
---------------------

The competition administrator can then create new tasks in the competition. The tasks and its basic
properties are created using the web interface. Related assets (test cases, or generator of these
test cases) can be uploaded. This upload, usually in a form of a ``.zip``, can also contain
configuration of test "batches" such as memory or time limits. These limits can then be edited
manually in the web interface after upload.


Creating and testing a submit (MVI)
-----------------------------------

When a task is ready, submits can be made. A testing submit can be created from the M3K frontend.
The competition web won't be notified of this submit, so it is useful for manual debugging. Normally
however, the submit is created by the competition web.

After the submit is received (over the API or from the frontend), it is scheduled for testing and
eventually picked by a runner. The following will then happen:

  - The runner will download (or load from cache if available) all Task assets (inputs / outputs,
    custom execute script, ...)
  - The runner will download all "Submit assets" (such as program to be tested).
  - Submit asset preparation will run, in a way specific for the type of the submit asset (e.g.
    ``text/x-c++`` will be compiled, ``text/x-python`` will be left as-is, ...)
  - Next steps will depend on what TestType the Task uses. Let's assume that the type is
    ``execute_and_diff``.
  - The prepared submit assets will be executed for each test case in the task. These test cases are
    grouped together in batches, and per-batch time and memory limits are enforced. Testing of a
    batch is stopped early if any test case in the batch fails.
  - Test results are returned to the backend, which will in turn notify the competition web (if
    applicable) via a webhook. (If this message gets lost, the client web can still query the submit
    status to get the result).


Viewing submits (MVI)
---------------------

After a submit is created, a Competition administrator can view details about this submit, including
(possibly partial) test results and submit assets.


Scheduling jobs (MVI)
---------------------

The system will distribute work (subclasses of :ref:`GenericJob`) to Workers, which will then be
reporting testing results (either partial or complete). After feasibility study and research of
existing tools and frameworks, it turns out, that for our use-case, that our custom implementation
of this mechanism is the best solution.

Since runners are communicating over HTTPS, they need to ask the controller whether there are any
jobs in queue. The controller then queries all :ref:`GenericJob` instances with ``scheduled_for``
lower or equal to the current time, sorted by priority and by ``scheduled_for`` (lowest values
first), and returns the first result to the Worker. Then the backend:

  - Increments the value of ``scheduled_for`` by a time constant. Therefore if a Worker dies, fails
    or loses connection, the job will be eventually, in the future, picked by another worker.
  - Sets job status to ``preparing``
  - Assigns ``Workers.id`` value to the job.

The worker then starts preparing to process the job (for example, by downloading required test
assets such as inputs/outputs, or a Docker image of an environment where the testing will run). When
that is done, job state will be changed to ``running``, the worker will start the testing and
sending partial results to the controller. After the processing finishes, it sends final results and
the following will happen:

  - Job status will be set to ``done``.
  - ``scheduled_for`` will be set to ``null``.

The administrator may also cancel a job, which prevents the job from being picked up by a worker in
the future (however it won't stop execution of the job if it has already been picked up).

If there is an error during execution of a job that won't be resolved by running it on another
worker, its state may be set to ``failed``.
